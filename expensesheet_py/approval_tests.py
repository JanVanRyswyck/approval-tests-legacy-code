from _pytest.capture import CaptureFixture
from approvaltests import *
from approvaltests.reporters import ReporterByCopyMoveCommandForEverythingToClipboard, \
    MultiReporter, PythonNativeReporter

from expense_sheet import ExpenseType, Expense, ExpenseSheet

options = (Options()
    .with_reporter(MultiReporter(
PythonNativeReporter(), ReporterByCopyMoveCommandForEverythingToClipboard())))


#
# Step 1: a first approval test
#
# Create an expense sheet for a single dinner expense with a fairly moderate amount. Use the `capsys` parameter to
# capture the output being written to `stdout` by the `print` function in the ExpenseSheet class. After that the output
# can be verified by calling `verify`. Visually check the output in the "received" file (see Approvals folder). When
# the received content matches the expectations, rename "received" to "approved" or sync the content to the "approved"
# file.
#
# Tip: Specify the `options` to the `verify` function so that the output files are written to the Approvals folder.
#
# Tip: Make sure that the test passes after approval. If you run into a date/time related issue, have a look
# at the following URL:
# https://github.com/approvals/ApprovalTests.Python/blob/main/docs/how_to/scrub_dates.md
#
# Tip: An easy way to approve the received output is to open a terminal, paste the command that is currently
# on the clipboard (Windows: CTRL+V or Mac: CMD+V) and execute it.
def test_print_dinner_expense(capsys: CaptureFixture) -> None:
    pass


# Step 2: a more comprehensive approval test
#
# One way to go forward is to write additional approval tests like the one in step 1 to cover every scenario for
# the `print` method. A more comprehensive way is to verify all scenarios in one go.
#
# Tip: Expand the `expense_scenarios` method for adding more scenarios.
#
# Tip: Specify the `options` to the `verify` function so that the output files are written to the Approvals folder.
#
# Tip: Make sure that the test passes after approval. If you run into a date/time related issue, have a look
# at the following URL:
# https://github.com/approvals/ApprovalTests.Python/blob/main/docs/how_to/scrub_dates.md
#
# Tip: Use a code coverage tool to verify whether all code paths have been executed through the implementation of
# the `print` method.
def test_print_expense_scenarios(capsys: CaptureFixture) -> None:
    verify_all("",
        expense_scenarios(),
        lambda expenses: "",  # TODO: call the `print` method and return the captured output
        options=options)


def expense_scenarios() -> list[list[Expense]]:
    dinner_of_100 = Expense(ExpenseType.DINNER, 100)

    #
    # TODO: Create more `Expense` instances as needed
    #

    return [
        [dinner_of_100],

        #
        # TODO: Add a list of one or multiple expenses for each scenario.
        #
    ]


# Step 3: a combinatorial approval test
#
# Let's try a different approach compared to step 2. Another way would be to use a combination of parameters.
# In our case we can create combinations of expense types and amounts. These combinations can be verified using
# `verify_all_combinations`.
#
# See the documentation for more information:
# https://github.com/approvals/ApprovalTests.Python/blob/main/docs/how_to/test_combinations_of_inputs.md
#
# Tip: Specify the `options` to the `verify` function so that the output files are written to the Approvals folder.
#
# Tip: Use a code coverage tool to verify whether all code paths have been executed through the implementation of
# the `print` method.
def test_print_expense_combinations(capsys: CaptureFixture) -> None:
    #
    # TODO: Provide the necessary expense types / amounts to be used to verify combinations.
    #
    expense_types = None
    amounts = None

    verify_all_combinations(
        lambda expense_type, amount: do_print_combination(expense_type, amount, capsys),
        [expense_types, amounts],
        options=options
    )


def do_print_combination(expense_type: ExpenseType, amount: int, capsys: CaptureFixture) -> str:
    #
    # TODO: Create an `Expense` instance using the specified parameters, call the `print` method on the `ExpenseSheet`
    # class and return the captured output.
    #
    return ""


# Step 6: take some time to review the tests for step 2 and step 3. What are the differences between these tests?
# Which approach is best suited in our case? Why?
