from dataclasses import dataclass
from datetime import datetime
from enum import Enum

class ExpenseType(Enum):
    DINNER = 0
    BREAKFAST = 1
    CAR_RENTAL = 2

@dataclass
class Expense:
    type: ExpenseType
    amount: int

class ExpenseSheet:

    def print(self, expenses: list[Expense]):
        total = 0
        meal_expenses = 0

        print("Expenses " + str(datetime.now()))

        for expense in expenses:
            if expense.type == ExpenseType.DINNER or expense.type == ExpenseType.BREAKFAST:
                meal_expenses += expense.amount

            expense_name = ""
            match expense.type:
                case ExpenseType.DINNER:
                    expense_name = "Dinner"
                case ExpenseType.BREAKFAST:
                    expense_name = "Breakfast"
                case ExpenseType.CAR_RENTAL:
                    expense_name = "Car Rental"

            meal_over_expenses_marker = "X" if expense.type == ExpenseType.DINNER and expense.amount > 5000 or expense.type == ExpenseType.BREAKFAST and expense.amount > 1000 else " "
            print(expense_name + "\t" + str(expense.amount) + "\t" + meal_over_expenses_marker)
            total += expense.amount

        print(f"Meal expenses: {meal_expenses}")
        print(f"Total expenses: {total}")