package be.principal.it;

import org.approvaltests.core.ApprovalFailureReporter;
import org.approvaltests.reporters.ClipboardReporter;
import org.approvaltests.reporters.DiffReporter;
import org.approvaltests.reporters.MultiReporter;

public class PackageSettings {
    public static String ApprovalBaseDirectory = "../resources";
    public static ApprovalFailureReporter UseReporter = new MultiReporter(new DiffReporter(), new ClipboardReporter());
}
