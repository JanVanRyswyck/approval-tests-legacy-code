package be.principal.it;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

    public List<String> makeItSo() {
        var result = new ArrayList<String>();

        for (int number=1; number<=15; number++) {
            if(number % 3 == 0 && number %5 == 0) {
                result.add("FizzBuzz");
            } else if(number % 3 == 0) {
                result.add("Fizz");
            } else if (number %5 == 0) {
                result.add("Buzz");
            } else {
                result.add(Integer.toString(number));
            }
        }

        return result;
    }
}