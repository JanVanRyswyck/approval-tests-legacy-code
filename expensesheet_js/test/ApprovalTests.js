const path = require('node:path');
const { EOL } = require('node:os');
const approvals = require('approvals');
const Scrubbers = require('approvals/lib/Scrubbers/Scrubbers').Scrubbers;
const { ExpenseSheet, ExpenseType } = require('../src/expenseSheet');
const OutputCatcher = require('./OutputCatcher');

const approvalsDirName = path.join(__dirname, 'approvals');

describe('Approval tests', function() {
   
    beforeEach(function() {
        //
        // Step 1: uncomment this code
        //
        // Unfortunately, the `print` method doesn't provide any return value. Instead, it directly calls
        // `console.log`. This implies that we need to somehow capture the output being written to the console.
        // The `OutputCatcher` module enables this for us. Have a look at the implementation in order to understand
        // how it works.
        
        //OutputCatcher.start();    
    });
    
    afterEach(function() {
        //
        // Step 2: uncomment this code
        //
        
        //OutputCatcher.stop();    
    });
    
    //
    // Step 3: a first approval test
    //
    // Create an expense sheet for a single dinner expense with a fairly moderate amount. Use the `getCapturedOutput`
    // method of the OutputCatcher to retrieve the output. After that the output can be verified by calling
    // `approvals.verifyAndScrub` (see https://github.com/approvals/Approvals.NodeJS#approvalsverifyandscrub).
    // Visually check the output in the "received" file (see `approvals` directory). When the received content matches
    // the expectations, rename "received" to "approved" or sync the content to the "approved" file.
    //
    // Tip: You can use the `approvalsDirName` variable to make sure that `approvals.verifyAndScrub` stores the
    // "received" and "approved" files in the `approvals` directory.
    //
    // Tip: Make sure that the test passes after approval. If you run into a date/time related issue, have a look
    // at the `expenseDateScrubber` function below. Use this date scrubber for `approvals.verifyAndScrub`. 
    
    
    it('should print a dinner expense', function() {
        
    });
    
    // Step 4: a more comprehensive approval test
    //
    // One way to go forward is to write additional approval tests like the one in step 3 to cover every scenario for
    // the `print` method. A more comprehensive way is to verify all scenarios in one go.
    //
    // Tip: Expand the `expenseScenarios` method for adding more scenarios.
    // Tip: Use a code coverage tool to verify whether all code paths have been executed through the implementation of
    // the `print` method.
    it('should print expense scenarios', function() {
        
        let output = '';
        expenseScenarios().forEach(expenses => {
            //
            // TODO: call the `print` method and append the captured output to the `output` variable.
            // Tip: You can also append a new-line using the `EOL` variable.
            //
        });
        
        // TODO: call `approvals.verifyAndScrub` for the accumulated `output`.
    });
    
    function expenseScenarios() {
        
        const dinnerOf100 = { type: ExpenseType.DINNER, amount: 100 };
        
        //
        // TODO: Create more `Expense` instances as needed
        //
        
        return [
            [ dinnerOf100 ],
            
            //
            // TODO: Add a list of one or multiple expenses for each scenario.
            //
        ];
    }
    
    function expenseDateScrubber() {
        // Wed Nov 15 2023 18:53:13 GMT+0100 (GMT+01:00)
        const regex = /[a-zA-Z]{3} [a-zA-Z]{3} \d{2} \d{4} \d{2}:\d{2}:\d{2} [a-zA-Z]{3}\+\d{4} \([a-zA-Z]{3}\+\d{2}:\d{2}\)/g
        return Scrubbers.createReqexScrubber(regex, t => `<date_${t}>`);
    }
});


