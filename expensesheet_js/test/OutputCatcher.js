const { stdout } = require('node:process');

let previousStdOutWrite = null;
let capturedOutput = '';

exports.start = function() {
    previousStdOutWrite = stdout.write.bind(stdout);
    stdout.write = (chunk, encoding, callback) => {
        capturedOutput += chunk;
        return true;
    };
}

exports.stop = function() {
    stdout.write = previousStdOutWrite;
    
    previousStdOutWrite = null;
    clearCapturedOutput();
}

exports.getCapturedOutput = function() {
    const result = capturedOutput;
    clearCapturedOutput();
    return result;
}

function clearCapturedOutput() {
    capturedOutput = '';
}