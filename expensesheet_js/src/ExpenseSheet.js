const { stdout } = require('node:process');

const ExpenseType =  {
    DINNER: 'dinner',
    BREAKFAST: 'breakfast',
    CAR_RENTAL: 'car_rental'
}

function print(expenses) {
    let total = 0;
    let mealExpenses = 0;

    console.log("Expenses " + new Date());

    expenses.forEach(expense => {
        if (expense.type === ExpenseType.DINNER || expense.type === ExpenseType.BREAKFAST) {
            mealExpenses += expense.amount;
        }

        let expenseName = "";
        switch (expense.type) {
            case ExpenseType.DINNER:
                expenseName = "Dinner";
                break;
            case ExpenseType.BREAKFAST:
                expenseName = "Breakfast";
                break;
            case ExpenseType.CAR_RENTAL:
                expenseName = "Car Rental";
                break;
        }

        let mealOverExpensesMarker = expense.type === ExpenseType.DINNER && expense.amount > 5000 || expense.type === ExpenseType.BREAKFAST && expense.amount > 1000 ? "X" : " ";

        console.log(expenseName + "\t" + expense.amount + "\t" + mealOverExpensesMarker);

        total += expense.amount;
    });

    console.log("Meal expenses: " + mealExpenses);
    console.log("Total expenses: " + total);
}

module.exports = {
    ExpenseSheet: { print },
    ExpenseType
}