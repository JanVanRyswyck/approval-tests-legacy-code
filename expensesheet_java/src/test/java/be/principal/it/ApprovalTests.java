package be.principal.it;

import org.approvaltests.Approvals;
import org.approvaltests.combinations.CombinationApprovals;
import org.approvaltests.core.Options;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ApprovalTests {

    private OutputCatcher outputCatcher;

    @BeforeEach
    public void before() {
        //
        // Step 1: uncomment this code
        //
        // Unfortunately, the `print` method doesn't provide any return value. Instead, it directly calls
        // `System.out.println`. This implies that we need to somehow capture the output being written to the console.
        // The `OutputCatcher` class enables this for us. Have a look at the implementation in order to understand how
        // it works.

        //outputCatcher = new OutputCatcher();
        //outputCatcher.start();
    }

    @AfterEach
    public void after() {
        //
        // Step 2: uncomment this code
        //

        //outputCatcher.stop();
    }

    //
    // Step 3: a first approval test
    //
    // Create an expense sheet for a single dinner expense with a fairly moderate amount. Use the `getCapturedOutput`
    // method of the OutputCatcher to retrieve the output. After that the output can be verified by calling
    // `Approvals.verify`. Visually check the output in the "received" file (see resources). When the received content
    // matches the expectations, rename "received" to "approved" or sync the content to the "approved" file.
    //
    // Tip: Make sure that the test passes after approval. If you run into a date/time related issue, have a look
    // at the following URL:
    // https://github.com/approvals/ApprovalTests.Java/blob/master/approvaltests/docs/Scrubbers.md#date-scrubbing
    //
    // Tip: An easy way to approve the received output is to open a terminal, paste the command that is currently
    // on the clipboard (Windows: CTRL+V or Mac: CMD+V) and execute it.
    @Test
    void printDinnerExpense() {

    }

    // Step 4: a more comprehensive approval test
    //
    // One way to go forward is to write additional approval tests like the one in step 3 to cover every scenario for
    // the `print` method. A more comprehensive way is to use `Approvals.verifyAll` to verify all scenarios in one go.
    //
    // See the documentation for more information:
    // https://github.com/approvals/ApprovalTests.Java/blob/master/approvaltests/docs/how_to/TestAVarietyOfValues.md#when-to-use-approvalsverifyall
    //
    // Tip: Expand the `expenseScenarios` method for adding more scenarios.
    //
    // Tip: Use a code coverage tool to verify whether all code paths have been executed through the implementation of
    // the `print` method.
    @Test
    void printExpenseScenarios() {

        var sut = new ExpenseSheet();

        Approvals.verifyAll(
            null,
            expenseScenarios(),
            expenses -> {
                //
                // TODO: call the `print` method and return the captured output
                //
                return "";
            },
            new Options()
        );
    }

    static List<List<Expense>> expenseScenarios() {

        var dinnerOf100 = new Expense();
        dinnerOf100.type = ExpenseType.DINNER;
        dinnerOf100.amount = 100;

        //
        // TODO: Create more `Expense` instances as needed
        //

        return List.of(
            List.of(dinnerOf100)
            //
            // TODO: Add a list of one or multiple expenses for each scenario.
            //
        );
    }

    // Step 5: a combinatorial approval test
    //
    // Let's try a different approach compared to step 4. Another way would be to use a combination of parameters.
    // In our case we can create combinations of expense types and amounts. These combinations can be verified using
    // `CombinationApprovals.verifyAllCombinations`.
    //
    // See the documentation for more information:
    // https://github.com/approvals/ApprovalTests.Java/blob/master/approvaltests/docs/how_to/TestCombinations.md#the-basics
    //
    // Tip: Use a code coverage tool to verify whether all code paths have been executed through the implementation of
    // the `print` method.
    @Test
    void printExpenseCombinations() {

        //
        // TODO: Provide the necessary expense types / amounts to be used to verify combinations.
        //
        ExpenseType[] expenseTypes = null;
        Integer[] amounts = null;

        CombinationApprovals.verifyAllCombinations(this::doPrint, expenseTypes, amounts, new Options());
    }

    private String doPrint(ExpenseType expenseType, int amount) {

        //
        // TODO: Create an `Expense` instance using the specified parameters, call the `print` method on the `ExpenseSheet`
        // class and return the captured output.
        //
        return "";
    }

    // Step 6: take some time to review the tests for step 4 and step 5. What are the differences between these tests?
    // Which approach is best suited in our case? Why?
}
