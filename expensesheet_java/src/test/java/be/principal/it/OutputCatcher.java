package be.principal.it;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class OutputCatcher {

    private PrintStream previousSysOut;
    private ByteArrayOutputStream outputStream;
    private PrintStream newSysOut;

    public void start() {
        previousSysOut = System.out;

        outputStream = new ByteArrayOutputStream();
        newSysOut = new PrintStream(outputStream, false, StandardCharsets.UTF_8);

        System.setOut(newSysOut);
    }

    public void stop() {
        System.setOut(previousSysOut);
        newSysOut.close();
    }

    public String getCapturedOutput() {
        var output = outputStream.toString(StandardCharsets.UTF_8);
        outputStream.reset();
        return output;
    }
}
